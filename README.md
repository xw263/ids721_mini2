# IDS721_Mini2
[![pipeline status](https://gitlab.com/xw263/ids721_mini2/badges/main/pipeline.svg)](https://gitlab.com/xw263/ids721_mini2/-/commits/main)


## Purpose
The goal of this project is to create an AWS Lambda function that processes data.

What this project does is to create a lambda function on AWS that count the number of characters in a string. 

## Steps
1. Rust and Cargo Lambda are required for this project: brew install rust, brew tap cargo-lambda/cargo-lambda, and brew install cargo-lambda

2. An AWS account is required. Sign up for the free tier.
3. Set up your Cargo Lambda project cargo lambda new new-lambda-project

4. Modify the template with your own additions or deletions.
5. Do cargo lambda watch to test locally.
6. Do cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }" to test the Lambda function (arguments passed in may be different).
7. Go to the AWS IAM web management console and add an IAM User for credentials.
8. Attach policies lambdafullaccess and iamfullaccess.
9. Finish user creation, open user details, and go to security credentials.
10. Generate access key.
11. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) in a .env file that won't be pushed with your repo (add to .gitignore).
12. Export the vars by using export in the terminal. This lets cargo lambda know which AWS account to deploy to.
13. Build your project for release cargo lambda build --release.
14. Deploy your project for release cargo lambda deploy.
15. Login to AWS Lambda (make sure you have the region correct on the top right bar) and check you have it installed.
16. After confirmation, you now want to connect your Lambda function with AWS API Gateway so head over there.
17. Create a new API (keep default settings) then create a new resource (the URL path that will be appended to your API link).
18. Select the resource, choose method type ANY and select Lambda function.
19. Turn on Lambda proxy integration.
20. Deploy your API by adding a new stage (another string that will be appended to your URL path).
21. After clicking deploy, find stages and find your invoke URL.

After everything is done, you will have a link like so:
https://a5xyttmdz1.execute-api.us-east-1.amazonaws.com/test/encrypt-decrypt
You can test your api gateway by sending a curl post request:

curl -X POST https://a5xyttmdz1.execute-api.us-east-1.amazonaws.com/test/encrypt-decrypt \
  -H 'content-type: application/json' \
  -d '{ "command": "decrypt", "message": "yVpcf4OnRrbQiGCaFh155A==" }'


You can test your lambda fucntion without using the api gateway like so:
cargo lambda invoke --remote <name of lambda function>

22. After confirming your api gateway and lambda functions work, add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) to gitlab secrets
23. Use my .gitlab-ci.yml to enable auto build, test, and deploy of your lambda function every time you push your repo


### Lambda Function Diagram Integration
![Screenshot_2024-02-03_at_6.39.47_PM](/uploads/991e9298b7872d962508b217e6e27e9c/Screenshot_2024-02-03_at_6.39.47_PM.png)

### Test Example
![Screenshot_2024-02-03_at_6.39.07_PM](/uploads/5c8569ae945349512903094215a891ab/Screenshot_2024-02-03_at_6.39.07_PM.png)

## Reference
1. https://gitlab.com/jeremymtan/jeremytan_ids721_week2
